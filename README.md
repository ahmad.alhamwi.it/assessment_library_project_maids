
# Library project

Demo project for Assessment as Back-end developer in Maids.cc company


- In this application all requirements have been implemented which are:  
  1.All EndPoints  
  2.Data Storage which is H2 database with jpa.  
  3.Validation and Error Handling.  
  4.Security with JWT.  
  5.Aspects using logging.  
  6.Caching.  
  7.Transaction Management.  
  8.Testing with Junit and Mockito.
<br>9.Tracing.

## Run The application
  - First clone this repository 
  - And make sure you have installed JDK 17
```bash
  java -version
```
- Then open this project with any IDE support, but I recommended    IntelliJ IDEA
- Then you will find the main class in the root folder

    
## API Reference

#### Get Jwt token
```http
  POST /login
```
##### Request Example
 - we have just one in memory user  
```
    {
        "username":"USER",
        "password":"PASSWORD"
    }
```
 - As result, you will get Jwt token in the response body 
      ,then you should insert the token in the headers of every     request you will send it later in the "Authorization" header

---

#### Get all book

```http
  GET /api/v1/books
```
- As result, you will get all the saved books as pages

---

#### Get One book information

```http
  GET /api/v1/books/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Long` | **Required**. Id of book to fetch |

---

#### Delete One book

```http
  DELETE /api/v1/books/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Long` | **Required**. Id of book to delete |

---

#### Update book information

```http
  PUT /api/v1/books/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Long` | **Required**. Id of book to delete |


##### Request Example

    {
    "title":"The father of HR",
    "author":"Andrew Maroun",
    "publicationYear": "2024-26-01 10:10:10"
    }          
---

#### Crete new book

```http
  POST /api/v1/books
```
##### Request Example

    {
    "title":"The father of HR",
    "author":"Andrew Maroun",
    "publicationYear": "2024-26-01 10:10:10"
    }

---

#### Get all patrons

```http
  GET /api/v1/patron
```
- As result, you will get all the saved patrons as pages

---

#### Get One patron information

```http
  GET /api/v1/patron/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Long` | **Required**. Id of patron to fetch |

---

#### Delete One patron

```http
  DELETE /api/v1/patron/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Long` | **Required**. Id of patron to delete |

---

#### Update patron information

```http
  PUT /api/v1/patron/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Long` | **Required**. Id of patron to delete |


##### Request Example

    {
    "name":"Ahmad",
    "contactInformation": "Syria - Damascus"
    }         
---

#### Crete new patron

```http
  POST /api/v1/books
```
##### Request Example

    {
    "name":"Ahmad",
    "contactInformation": "Syria - Damascus"
    } 

---

#### Crete new borrow

```http
  POST /api/v1/borrow/${bookId}/patron/${patronId}
```

  | Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `bookId`      | `Long` | **Required**. Id of book  |
| `patronId`      | `Long` | **Required**. Id of patron  |