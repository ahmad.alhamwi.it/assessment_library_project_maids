package cc.maids.assessment;

import cc.maids.assessment.controllers.BookController;
import cc.maids.assessment.dto.PostBookDTO;
import cc.maids.assessment.dto.ReturnBookDTO;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import cc.maids.assessment.services.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BookControllerTest {

    @Mock
    private BookService bookService;

    @InjectMocks
    private BookController bookController;

    private PostBookDTO postBookDTO;
    private ReturnBookDTO returnBookDTO;

    @BeforeEach
    public void setup() {
        postBookDTO = new PostBookDTO();
        returnBookDTO = new ReturnBookDTO();
    }

    @Test
    public void testAddBook() {
        // Mocking
        when(bookService.addNewBook(postBookDTO)).thenReturn(returnBookDTO);

        // Execution
        ResponseEntity<?> responseEntity = bookController.addBook(postBookDTO);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }

    @Test
    public void testGetAllBooks() {
        // Mocking
        Page<ReturnBookDTO> returnBookDTOS = mock(Page.class);
        when(bookService.getAllBooks(any(Pageable.class))).thenReturn(returnBookDTOS);

        // Execution
        ResponseEntity<?> responseEntity = bookController.getAllBooks(Pageable.unpaged());

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }

    @Test
    public void testGetBookById() throws ResourceNotFoundException {
        // Mocking
        Long bookId = 1L;
        when(bookService.getBook(bookId)).thenReturn(returnBookDTO);

        // Execution
        ResponseEntity<?> responseEntity = bookController.getBookById(bookId);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }

    @Test
    public void testUpdateBookById() throws ResourceNotFoundException {
        // Mocking
        Long bookId = 1L;
        when(bookService.updateBookInformation(bookId, postBookDTO)).thenReturn(returnBookDTO);

        // Execution
        ResponseEntity<?> responseEntity = bookController.getUpdateBookById(bookId, postBookDTO);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }

    @Test
    public void testDeleteBook() throws ResourceNotFoundException {
        // Mocking
        Long bookId = 1L;
        String returnMsg = "Book deleted successfully";
        when(bookService.deleteBook(bookId)).thenReturn(returnMsg);

        // Execution
        ResponseEntity<?> responseEntity = bookController.deleteBook(bookId);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }
}
