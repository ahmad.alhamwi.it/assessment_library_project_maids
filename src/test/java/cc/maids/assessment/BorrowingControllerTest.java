package cc.maids.assessment;

import cc.maids.assessment.controllers.BorrowingController;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import cc.maids.assessment.services.BorrowingService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BorrowingControllerTest {

    @Mock
    private BorrowingService borrowingService;

    @InjectMocks
    private BorrowingController borrowingController;

    @Test
    public void testBorrowBook() throws ResourceNotFoundException {
        // Mocking
        Long bookId = 1L;
        Long patronId = 1L;
        HashMap<Object, Object> returnData = new HashMap<>();
        returnData.put("obj", null);
        returnData.put("msg", "Book borrowed successfully");
        when(borrowingService.borrowBook(bookId, patronId)).thenReturn(returnData);

        // Execution
        ResponseEntity<?> responseEntity = borrowingController.borrowBook(bookId, patronId);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }

    @Test
    public void testReturnBook() throws ResourceNotFoundException {
        // Mocking
        Long bookId = 1L;
        Long patronId = 1L;
        HashMap<Object, Object> returnData = new HashMap<>();
        returnData.put("obj", null);
        returnData.put("msg", "Book returned successfully");
        when(borrowingService.returnBook(bookId, patronId)).thenReturn(returnData);

        // Execution
        ResponseEntity<?> responseEntity = borrowingController.returnBook(bookId, patronId);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }
}


