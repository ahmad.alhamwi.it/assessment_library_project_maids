package cc.maids.assessment;

import cc.maids.assessment.controllers.PatronController;
import cc.maids.assessment.dto.PostPatronDTO;
import cc.maids.assessment.dto.ReturnPatronDTO;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import cc.maids.assessment.payload.ApiResponse;
import cc.maids.assessment.services.PatronService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PatronControllerTest {

    @Mock
    private PatronService patronService;

    @InjectMocks
    private PatronController patronController;

    @Test
    public void testAddPatron() {
        // Mocking
        PostPatronDTO postPatronDTO = new PostPatronDTO();
        ReturnPatronDTO returnPatronDTO = new ReturnPatronDTO();
        when(patronService.addNewPatron(postPatronDTO)).thenReturn(returnPatronDTO);

        // Execution
        ResponseEntity<?> responseEntity = patronController.addPatron(postPatronDTO);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }

    @Test
    public void testGetAllPatrons() {
        // Mocking
        Page<ReturnPatronDTO> returnPatronDTOS = mock(Page.class);
        when(patronService.getAllPatrons(any(Pageable.class))).thenReturn(returnPatronDTOS);

        // Execution
        ResponseEntity<?> responseEntity = patronController.getAllPatrons(Pageable.unpaged());

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }

    @Test
    public void testGetPatronById() throws ResourceNotFoundException {
        // Mocking
        Long patronId = 1L;
        ReturnPatronDTO returnPatronDTO = new ReturnPatronDTO();
        when(patronService.getPatron(patronId)).thenReturn(returnPatronDTO);

        // Execution
        ResponseEntity<?> responseEntity = patronController.getPatronById(patronId);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }

    @Test
    public void testUpdatePatronById() throws ResourceNotFoundException {
        // Mocking
        Long patronId = 1L;
        PostPatronDTO updatedPatronDTO = new PostPatronDTO();
        ReturnPatronDTO returnPatronDTO = new ReturnPatronDTO();
        when(patronService.updatePatronInformation(patronId, updatedPatronDTO)).thenReturn(returnPatronDTO);

        // Execution
        ResponseEntity<?> responseEntity = patronController.getPatronById(patronId, updatedPatronDTO);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }

    @Test
    public void testDeletePatron() throws ResourceNotFoundException {
        // Mocking
        Long patronId = 1L;
        String returnMsg = "Patron deleted successfully";
        when(patronService.deletePatron(patronId)).thenReturn(returnMsg);

        // Execution
        ResponseEntity<?> responseEntity = patronController.deletePatron(patronId);

        // Assertions
        assertEquals(200, responseEntity.getStatusCodeValue());
        // Add more assertions if necessary
    }
}

