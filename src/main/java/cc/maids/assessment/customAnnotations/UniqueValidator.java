package cc.maids.assessment.customAnnotations;

import cc.maids.assessment.repositories.BookRepo;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;



public class UniqueValidator implements ConstraintValidator<Unique, String> {
    @Autowired
    private BookRepo bookRepo;

    @Autowired
    public UniqueValidator(BookRepo productRepo) {
        this.bookRepo = productRepo;
    }

    // Default no-argument constructor for Spring to use
    public UniqueValidator() {
    }

    @Override
    public void initialize(Unique constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !bookRepo.existsByTitle(value);
    }
}

