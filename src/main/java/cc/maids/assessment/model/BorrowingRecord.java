package cc.maids.assessment.model;

import cc.maids.assessment.tracing.Auditable;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "borrowingRecord_entity")
public class BorrowingRecord extends Auditable {
    @Id
    @GeneratedValue(
            generator = "borrowingRecord_sequence",
            strategy = GenerationType.SEQUENCE
    )    @SequenceGenerator(
            name = "borrowingRecord_sequence",
            sequenceName = "BorrowingRecord_sequence",
            allocationSize = 1
    )
    private Long recordID;

    @ManyToOne
    @JoinColumn(name = "bookID", nullable = false)
    private Book book;

    @ManyToOne
    @JoinColumn(name = "patronID", nullable = false)
    private Patron patron;

    private LocalDateTime borrowingDate;
    private LocalDateTime returnDate;
}
