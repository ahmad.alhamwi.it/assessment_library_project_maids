package cc.maids.assessment.model;

import cc.maids.assessment.tracing.Auditable;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "patron_entity")
public class Patron extends Auditable {
    @Id
    @GeneratedValue(
            generator = "patron_sequence",
            strategy = GenerationType.SEQUENCE
    )
    @SequenceGenerator(
            name = "patron_sequence",
            sequenceName = "patron_sequence",
            allocationSize = 1
    )
    private Long patronID;
    private String name;
    private String contactInformation;
}
