package cc.maids.assessment.model;

import cc.maids.assessment.tracing.Auditable;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.util.Date;


@Entity
@Audited
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "book_entity")
public class Book extends Auditable {
    @Id
    @GeneratedValue(
            generator = "book_sequence",
            strategy = GenerationType.SEQUENCE
    )
    @SequenceGenerator(
            name = "book_sequence",
            sequenceName = "book_sequence",
            allocationSize = 1
    )
    @Column(name = "book_id")
    private Long bookID;
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "author", nullable = false)
    private String author;
    @Column(name = "publication_Year")
    private Date publicationYear;
    @Column(name = "iSBN")
    private String ISBN;

}
