package cc.maids.assessment.controllers;

import cc.maids.assessment.dto.PostPatronDTO;
import cc.maids.assessment.dto.ReturnPatronDTO;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import cc.maids.assessment.payload.ApiResponse;
import cc.maids.assessment.services.PatronService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;

@RestController
@RequestMapping("/api/v1/patron")
public class PatronController {
    private final PatronService patronService;

    @Autowired
    public PatronController(PatronService patronService) {
        this.patronService = patronService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addPatron(@Valid @RequestBody PostPatronDTO newPatron) {
        ReturnPatronDTO returnPatronDTO = patronService.addNewPatron(newPatron);
        ApiResponse<ReturnPatronDTO> apiResponse = new ApiResponse<>(returnPatronDTO, "Patron added successfully", LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllPatrons(Pageable pageable) {
        Page<ReturnPatronDTO> returnPatronDTO = patronService.getAllPatrons(pageable);
        ApiResponse<Page<ReturnPatronDTO>> apiResponse = new ApiResponse<>(returnPatronDTO, "Information of All Patrons", LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{patronId}")
    public ResponseEntity<?> getPatronById(@PathVariable Long patronId) throws ResourceNotFoundException {
        ReturnPatronDTO returnPatronDTO = patronService.getPatron(patronId);
        ApiResponse<ReturnPatronDTO> apiResponse = new ApiResponse<>(returnPatronDTO, "Information of Patron with ID:" + patronId, LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{patronId}")
    public ResponseEntity<?> getPatronById(@PathVariable Long patronId,@Valid @RequestBody PostPatronDTO updatedPatron) throws ResourceNotFoundException {
        ReturnPatronDTO returnPatronDTO = patronService.updatePatronInformation(patronId, updatedPatron);
        ApiResponse<ReturnPatronDTO> apiResponse = new ApiResponse<>(returnPatronDTO, "Updated Information of Patron with ID:" + patronId, LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{patronId}")
    public ResponseEntity<?> deletePatron(@PathVariable Long patronId) throws ResourceNotFoundException {
        String returnMsg = patronService.deletePatron(patronId);
        ApiResponse<HashMap<Object, Object>> apiResponse = new ApiResponse<>(new HashMap<>(), returnMsg, LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }
}
