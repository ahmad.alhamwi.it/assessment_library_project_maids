package cc.maids.assessment.controllers;


import cc.maids.assessment.dto.LoginDto;
import cc.maids.assessment.dto.LoginResponse;
import cc.maids.assessment.services.impl.AuthImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final AuthImpl authImpl;

    public AuthController(AuthImpl authImpl) {
        this.authImpl = authImpl;
    }

    @PostMapping("/login")
    public LoginResponse authenticateUser(@RequestBody LoginDto loginDto) {
        return authImpl.login(loginDto);
    }
}
