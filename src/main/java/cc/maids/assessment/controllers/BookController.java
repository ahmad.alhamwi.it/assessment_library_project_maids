package cc.maids.assessment.controllers;

import cc.maids.assessment.payload.ApiResponse;
import cc.maids.assessment.dto.PostBookDTO;
import cc.maids.assessment.dto.ReturnBookDTO;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import cc.maids.assessment.services.BookService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Validated
@RestController
@RequestMapping("/api/v1/books")
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addBook(@Valid @RequestBody PostBookDTO newBook) {
        ReturnBookDTO returnBookDTO = bookService.addNewBook(newBook);
        ApiResponse<ReturnBookDTO> apiResponse = new ApiResponse<>(returnBookDTO, "Book added successfully", LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllBooks(Pageable pageable) {
        Page<ReturnBookDTO> returnBookDTOS = bookService.getAllBooks(pageable);
        ApiResponse<Page<ReturnBookDTO>> apiResponse = new ApiResponse<>(returnBookDTOS, "Information of All Books", LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{bookId}")
    public ResponseEntity<?> getBookById(@PathVariable Long bookId) throws ResourceNotFoundException {
        ReturnBookDTO returnBookDTO = bookService.getBook(bookId);
        ApiResponse<ReturnBookDTO> apiResponse = new ApiResponse<>(returnBookDTO, "Information of Book with ID:" + bookId, LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{bookId}")
    public ResponseEntity<?> getUpdateBookById(@PathVariable Long bookId,@Valid @RequestBody PostBookDTO book) throws ResourceNotFoundException {
        ReturnBookDTO returnBookDTO = bookService.updateBookInformation(bookId, book);
        ApiResponse<ReturnBookDTO> apiResponse = new ApiResponse<>(returnBookDTO, "Updated Information of Book with ID:" + bookId, LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{bookId}")
    public ResponseEntity<?> deleteBook(@PathVariable Long bookId) throws ResourceNotFoundException {
        String returnMsg = bookService.deleteBook(bookId);
        ApiResponse<HashMap<Object, Object>> apiResponse = new ApiResponse<>(new HashMap<>(), returnMsg, LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }
}
