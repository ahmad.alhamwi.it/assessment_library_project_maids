package cc.maids.assessment.controllers;

import cc.maids.assessment.payload.ApiResponse;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import cc.maids.assessment.services.BorrowingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;

@RestController
@RequestMapping("/api/v1/borrow")
public class BorrowingController {

    private final BorrowingService borrowingService;

    @Autowired
    public BorrowingController(BorrowingService borrowingService) {
        this.borrowingService = borrowingService;
    }

    @RequestMapping(method = RequestMethod.POST, path = "{bookId}/patron/{patronId}")
    public ResponseEntity<?> borrowBook(@PathVariable Long bookId, @PathVariable Long patronId) throws ResourceNotFoundException {
        HashMap<Object, Object> returnData = borrowingService.borrowBook(bookId, patronId);
        ApiResponse<Object> apiResponse =
                new ApiResponse<>(returnData.get("obj"), returnData.get("msg").toString(), LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{bookId}/patron/{patronId}")
    public ResponseEntity<?> returnBook(@PathVariable Long bookId, @PathVariable Long patronId) throws ResourceNotFoundException {
        HashMap<Object, Object> returnData = borrowingService.returnBook(bookId, patronId);
        ApiResponse<Object> apiResponse =
                new ApiResponse<>(returnData.get("obj"), returnData.get("msg").toString(), LocalDateTime.now());
        return ResponseEntity.ok(apiResponse);

    }
}
