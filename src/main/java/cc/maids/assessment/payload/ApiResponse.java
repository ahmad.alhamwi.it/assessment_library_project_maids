package cc.maids.assessment.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ApiResponse<T> {
    T data;
    String message;
    LocalDateTime timestamp;
}
