package cc.maids.assessment.repositories;

import cc.maids.assessment.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface BookRepo extends JpaRepository<Book, Long> , PagingAndSortingRepository<Book, Long> {

    boolean existsByTitle(String value);
}
