package cc.maids.assessment.repositories;

import cc.maids.assessment.model.Book;
import cc.maids.assessment.model.BorrowingRecord;
import cc.maids.assessment.model.Patron;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface BorrowRepo extends JpaRepository<BorrowingRecord,Long> , PagingAndSortingRepository<BorrowingRecord, Long> {

    @Transactional
    Optional<BorrowingRecord> findBorrowingRecordByBookEqualsAndPatronEqualsAndReturnDateIsNullAndBorrowingDateIsNotNull(Book book, Patron patron);

    @Transactional
    Optional<BorrowingRecord> findBorrowingRecordByBookEqualsAndPatronAndBorrowingDateIsNotNull(Book book, Patron patron);

    @Transactional
    Optional<BorrowingRecord> findBorrowingRecordByBookEqualsAndPatronAndBorrowingDateIsNotNullAndReturnDateIsNotNull(Book book, Patron patron);


}
