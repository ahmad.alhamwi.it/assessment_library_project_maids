package cc.maids.assessment.repositories;

import cc.maids.assessment.model.Patron;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface PatronRepo extends JpaRepository<Patron, Long>, PagingAndSortingRepository<Patron, Long> {
    Optional<Patron> findByName(String name);

}
