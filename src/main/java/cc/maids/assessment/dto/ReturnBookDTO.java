package cc.maids.assessment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReturnBookDTO {
    private Long bookID;
    private String title;
    private String author;
    private Date publicationYear;
    private String isbn;
}
