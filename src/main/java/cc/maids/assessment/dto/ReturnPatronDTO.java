package cc.maids.assessment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReturnPatronDTO {
    private Long patronID;
    private String name;
    private String contactInformation;
}
