package cc.maids.assessment.dto;

public record LoginResponse(String username, String jwt) {
}
