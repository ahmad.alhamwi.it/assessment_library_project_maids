package cc.maids.assessment.dto;

import cc.maids.assessment.model.Book;
import cc.maids.assessment.model.Patron;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReturnBorrowingRecordDTO {
    private Book book;
    private Patron patron;
    private LocalDateTime borrowingDate;
    private LocalDateTime returnDate;
}
