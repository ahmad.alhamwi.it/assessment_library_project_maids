package cc.maids.assessment.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostPatronDTO {
    @NotBlank(message = "Name cannot be blank")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Name must match \"[a-zA-Z\\s]\"")
    private String name;
    @NotBlank(message = "Contact Information cannot be blank")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Contact Information must match \"[a-zA-Z\\s]\"")
    private String contactInformation;
}
