package cc.maids.assessment.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostBookDTO {
    @NotBlank(message = "Book title cannot be blank")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Title must  match \"[a-zA-Z\\s]\"")
    private String title;
    @NotBlank(message = "Book author cannot be blank")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Author must match \"[a-zA-Z\\s]\"")
    private String author;
    @NotNull(message = "Publication year cannot be blank")
    @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}", message = "Invalid date and time format. Please use yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String publicationYear;
}
