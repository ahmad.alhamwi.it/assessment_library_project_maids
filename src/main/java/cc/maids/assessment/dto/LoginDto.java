package cc.maids.assessment.dto;

public record LoginDto(String username, String password) {
}
