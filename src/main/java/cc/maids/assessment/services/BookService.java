package cc.maids.assessment.services;

import cc.maids.assessment.dto.PostBookDTO;
import cc.maids.assessment.dto.ReturnBookDTO;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BookService {
    ReturnBookDTO updateBookInformation(Long bookId, PostBookDTO updatedBook) throws ResourceNotFoundException;

    Page<ReturnBookDTO> getAllBooks(Pageable pageable);

    ReturnBookDTO getBook(Long bookId) throws ResourceNotFoundException;

    ReturnBookDTO addNewBook(PostBookDTO newBook);

    String deleteBook(Long bookId) throws ResourceNotFoundException;
}