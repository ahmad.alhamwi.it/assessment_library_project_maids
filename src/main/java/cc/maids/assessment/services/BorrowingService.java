package cc.maids.assessment.services;

import cc.maids.assessment.exceptions.ResourceNotFoundException;

import java.util.HashMap;

public interface BorrowingService {
     HashMap<Object, Object> returnBook(Long bookId, Long patronId) throws ResourceNotFoundException;

     HashMap<Object, Object> borrowBook(Long bookId, Long patronId) throws ResourceNotFoundException;

}
