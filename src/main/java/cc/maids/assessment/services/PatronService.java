package cc.maids.assessment.services;

import cc.maids.assessment.dto.PostPatronDTO;
import cc.maids.assessment.dto.ReturnPatronDTO;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface PatronService {
    ReturnPatronDTO addNewPatron(PostPatronDTO newPatron);

    ReturnPatronDTO getPatron(Long patronId) throws ResourceNotFoundException;

    Page<ReturnPatronDTO> getAllPatrons(Pageable pageable);

    ReturnPatronDTO updatePatronInformation(Long patronId, PostPatronDTO updatedPatron) throws ResourceNotFoundException;

    String deletePatron(Long PatronId) throws ResourceNotFoundException;
}
