
package cc.maids.assessment.services.impl;

import cc.maids.assessment.dto.PostPatronDTO;
import cc.maids.assessment.dto.ReturnBookDTO;
import cc.maids.assessment.model.Book;
import cc.maids.assessment.model.Patron;
import cc.maids.assessment.repositories.PatronRepo;
import cc.maids.assessment.dto.ReturnPatronDTO;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import cc.maids.assessment.services.PatronService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatronServiceImp implements PatronService {
    private final PatronRepo patronRepo;
    private final ModelMapper modelMapper;

    @Autowired
    public PatronServiceImp(PatronRepo patronRepo, ModelMapper modelMapper) {
        this.patronRepo = patronRepo;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public ReturnPatronDTO addNewPatron(PostPatronDTO newPatron) {
        Patron patronRequest = modelMapper.map(newPatron, Patron.class);
        Patron savedPatron = patronRepo.save(patronRequest);
        return modelMapper.map(savedPatron, ReturnPatronDTO.class);
    }

    @Cacheable(value = "patrons", key = "#patronId")
    public ReturnPatronDTO getPatron(Long patronId) throws ResourceNotFoundException {
        Optional<Patron> optionalPatron = patronRepo.findById(patronId);
        if (optionalPatron.isPresent()) {
            return modelMapper.map(optionalPatron.get(), ReturnPatronDTO.class);
        } else {
            throw new ResourceNotFoundException("Patron with ID:" + patronId + " not found");
        }
    }

    @Cacheable(value = "patrons")
    public Page<ReturnPatronDTO> getAllPatrons(Pageable pageable) {
        Page<Patron> books = patronRepo.findAll(pageable);
        return books.map(post -> modelMapper.map(post, ReturnPatronDTO.class));

    }

    @Transactional
    @CachePut(value = "patrons", key = "#patronId")
    public ReturnPatronDTO updatePatronInformation(Long patronId, PostPatronDTO updatedPatron) throws ResourceNotFoundException {
        Optional<Patron> optionalPatron = patronRepo.findById(patronId);
        if (optionalPatron.isEmpty()) {
            throw new ResourceNotFoundException("Patron with ID:" + patronId + " not found");
        } else {
            Patron patron = optionalPatron.get();
            patron.setName(patron.getName());
            patron.setContactInformation(patron.getContactInformation());
            Patron savedPatron = patronRepo.save(patron);
            return modelMapper.map(savedPatron, ReturnPatronDTO.class);
        }
    }

    @Transactional
    public String deletePatron(Long patronId) throws ResourceNotFoundException {
        Optional<Patron> optionalPatron = patronRepo.findById(patronId);
        if (optionalPatron.isEmpty()) {
            throw new ResourceNotFoundException("Patron with ID:" + patronId + " not found");
        } else {
            patronRepo.deleteById(patronId);
            return "Patron with ID: " + patronId + "deleted successfully";
        }
    }


}
