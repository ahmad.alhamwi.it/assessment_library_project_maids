package cc.maids.assessment.services.impl;

import cc.maids.assessment.dto.PostBookDTO;
import cc.maids.assessment.model.Book;
import cc.maids.assessment.dto.ReturnBookDTO;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import cc.maids.assessment.repositories.BookRepo;
import cc.maids.assessment.services.BookService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BookServiceImp implements BookService {
    private final BookRepo bookRepo;
    private final ModelMapper modelMapper;

    @Autowired
    public BookServiceImp(BookRepo bookRepo, ModelMapper modelMapper) {
        this.bookRepo = bookRepo;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public ReturnBookDTO addNewBook(PostBookDTO newBook) {
        Book bookRequest = modelMapper.map(newBook, Book.class);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            bookRequest.setPublicationYear(sdf.parse(newBook.getPublicationYear()));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        bookRequest.setISBN(UUID.randomUUID().toString());
        Book savedBook = bookRepo.save(bookRequest);
        return modelMapper.map(savedBook, ReturnBookDTO.class);
    }

    @Cacheable(value = "books", key = "#bookId")
    public ReturnBookDTO getBook(Long bookId) throws ResourceNotFoundException {
        Optional<Book> optionalBook = bookRepo.findById(bookId);
        if (optionalBook.isPresent()) {
            return modelMapper.map(optionalBook.get(), ReturnBookDTO.class);
        } else {
            throw new ResourceNotFoundException("Book with ID: " + bookId + " not found");
        }

    }

    @Cacheable(value = "books")
    public Page<ReturnBookDTO> getAllBooks(Pageable pageable) {
        Page<Book> books = bookRepo.findAll(pageable);
        return books.map(post -> modelMapper.map(post, ReturnBookDTO.class));

    }

    @Transactional
    @CachePut(value = "books", key = "#bookId")
    public ReturnBookDTO updateBookInformation(Long bookId, PostBookDTO updatedBook) throws ResourceNotFoundException {
        Optional<Book> optionalBook = bookRepo.findById(bookId);
        if (optionalBook.isEmpty()) {
            throw new ResourceNotFoundException("Book with ID: " + bookId + " not found");
        } else {
            Book book = optionalBook.get();
            book.setTitle(updatedBook.getTitle());
            book.setAuthor(updatedBook.getAuthor());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                book.setPublicationYear(sdf.parse(updatedBook.getPublicationYear()));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            Book savedBook = bookRepo.save(book);
            return modelMapper.map(savedBook, ReturnBookDTO.class);
        }
    }

    @Transactional
    public String deleteBook(Long bookId) throws ResourceNotFoundException {
        Optional<Book> optionalBook = bookRepo.findById(bookId);
        if (optionalBook.isEmpty()) {
            throw new ResourceNotFoundException("Book with ID: " + bookId + " not found");
        } else {
            bookRepo.deleteById(bookId);
            return "Book with ID: " + bookId + "deleted successfully";
        }
    }

}
