package cc.maids.assessment.services.impl;

import cc.maids.assessment.model.BorrowingRecord;
import cc.maids.assessment.dto.ReturnBorrowingRecordDTO;
import cc.maids.assessment.exceptions.ResourceNotFoundException;
import cc.maids.assessment.model.Book;
import cc.maids.assessment.model.Patron;
import cc.maids.assessment.repositories.BookRepo;
import cc.maids.assessment.repositories.BorrowRepo;
import cc.maids.assessment.repositories.PatronRepo;
import cc.maids.assessment.services.BorrowingService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class BorrowingServiceImp implements BorrowingService {

    private final BookRepo bookRepo;
    private final PatronRepo patronRepo;
    private final BorrowRepo borrowRepo;
    private final ModelMapper modelMapper;


    @Autowired
    public BorrowingServiceImp(BookRepo bookRepo, PatronRepo patronRepo, BorrowRepo borrowRepo, ModelMapper modelMapper) {
        this.bookRepo = bookRepo;
        this.patronRepo = patronRepo;
        this.borrowRepo = borrowRepo;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public HashMap<Object, Object> borrowBook(Long bookId, Long patronId) throws ResourceNotFoundException {
        Optional<Book> book = bookRepo.findById(bookId);
        Optional<Patron> patron = patronRepo.findById(patronId);
        HashMap<Object, Object> returnHashMap = new HashMap<>();

        if (book.isEmpty()) {
            throw new ResourceNotFoundException("Book with ID: " + bookId + " not found");
        }
        if (patron.isEmpty()) {
            throw new ResourceNotFoundException("Patron with ID:" + patronId + " not found");
        }

        borrowRepo.findBorrowingRecordByBookEqualsAndPatronAndBorrowingDateIsNotNull(book.get(), patron.get()).ifPresent(
                record -> {
                    returnHashMap.put("msg", "The book is already borrowed");
                    returnHashMap.put("obj", modelMapper.map(record, ReturnBorrowingRecordDTO.class));
                }
        );
        if (!returnHashMap.isEmpty()) return returnHashMap;

        BorrowingRecord borrowingRecord = new BorrowingRecord();
        borrowingRecord.setBorrowingDate(LocalDateTime.now());
        borrowingRecord.setBook(book.get());
        borrowingRecord.setPatron(patron.get());
        borrowRepo.save(borrowingRecord);
        returnHashMap.put("msg", "Borrow book of ID: " + bookId + "successfully done");
        returnHashMap.put("obj", modelMapper.map(borrowingRecord, ReturnBorrowingRecordDTO.class));
        return returnHashMap;
    }


    @Transactional
    public HashMap<Object, Object> returnBook(Long bookId, Long patronId) throws ResourceNotFoundException {
        Optional<Book> book = bookRepo.findById(bookId);
        Optional<Patron> patron = patronRepo.findById(patronId);

        if (book.isEmpty()) {
            throw new ResourceNotFoundException("Book with ID: " + bookId + " not found");
        }
        if (patron.isEmpty()) {
            throw new ResourceNotFoundException("Patron with ID:" + patronId + " not found");
        }
        if (borrowRepo.findBorrowingRecordByBookEqualsAndPatronAndBorrowingDateIsNotNull(book.get(), patron.get()).isEmpty()) {
            throw new ResourceNotFoundException("Borrowing Record not found");
        }

        AtomicReference<BorrowingRecord> borrowingRecord = new AtomicReference<>();
        borrowRepo.findBorrowingRecordByBookEqualsAndPatronEqualsAndReturnDateIsNullAndBorrowingDateIsNotNull(book.get(), patron.get()).ifPresent(
                record -> {
                    record.setReturnDate(LocalDateTime.now());
                    borrowRepo.save(record);
                    borrowingRecord.set(record);
                }
        );
        HashMap<Object, Object> hashMap = new HashMap<>();
        if (borrowingRecord.get() != null) {
            hashMap.put("msg", "Return book done successfully");
            hashMap.put("obj", modelMapper.map(borrowingRecord.get(), ReturnBorrowingRecordDTO.class));
        } else {
            Optional<BorrowingRecord> record =
                    borrowRepo.findBorrowingRecordByBookEqualsAndPatronAndBorrowingDateIsNotNullAndReturnDateIsNotNull(book.get(), patron.get());
            hashMap.put("msg", "The book is already returned");
            hashMap.put("obj", modelMapper.map(record.get(), ReturnBorrowingRecordDTO.class));
        }
        return hashMap;

    }
}



